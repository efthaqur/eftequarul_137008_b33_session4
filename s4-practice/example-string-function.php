<?php 
	$br = "<br />";
	$text = "He said \"Hello O'Reilly\" & disappeared.\nNext line...";
	$text1 = "He said \"Hello O'Reilly\" & disappeared.\nNext line...";

	echo $text; #Prints value of $text without slash
	echo $br;
	echo addslashes($text1); #Prints value with slashes

	echo $br;

	$text2 = 'Hello, world, Hey, Dave!'; // A string is declared
	$text3 = explode(',', $text2); //explode function showing it as array

	print_r($text3); #shows $text3 as array

	echo $br;
	$text4 = array('Scroll', 'Lock', 'Pause'); //Array is decalred
	$text5 = implode(' * ',$text4); #implode funtion making array as string
	var_dump($text5); 

	echo $br;
	echo ENT_HTML5;
	echo $br;
	echo ENT_HTML401;

	echo $br;

	//Example of htmlentitles
	$a = "This is example of HTML Tag
			<p>Hello! This is bold text</p>
 			<i>I am using HTML</i>";

	$b = htmlentities($a);
	echo "<pre>";
	echo $b;
	echo "</pre>";
	echo $br;
	echo nl2br("This is string first line \n This is second line");

	echo str_repeat(" php ",20); # This will repeat php string for 20 times

	echo $br;
	$str1 = "Hello! Hunny Bunny!!";

	echo strtolower($str1);
?>